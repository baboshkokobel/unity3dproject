﻿using UnityEngine;
using System.Collections;

public class CameraShoot : MonoBehaviour {

	public float shotSpeed = 1000f;
	public Rigidbody bullet;
	public float speed = 30f;
	
	// Update is called once per frame
	void Update () {
		
		float hAxis = Input.GetAxis ("Vertical");
		float xAxis = Input.GetAxis ("Horizontal");
		
		transform.position += new Vector3 (0f, hAxis*Time.deltaTime*speed, 0f);
		transform.Rotate(0f, xAxis * Time.deltaTime * speed, 0f);
		if (Input.GetButtonDown ("Fire1")) {

			Rigidbody bulletOne = (Rigidbody) Instantiate(bullet,transform.position,transform.rotation);
			bulletOne.AddForce(transform.forward * shotSpeed);
			Destroy(bulletOne.gameObject,1f);

		}
	
	}
}
