﻿using UnityEngine;
using System.Collections;

public class CubeMove : MonoBehaviour {

	public float speed = 5f;
	
	// Update is called once per frame
	void Update () {
		float hAxis = Input.GetAxis ("Vertical");
		float xAxis = Input.GetAxis ("Horizontal");

		transform.position += new Vector3 (xAxis*Time.deltaTime*speed, hAxis*Time.deltaTime*speed, 0f);
	}
}
