﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour {

	public float speed =15f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float hAxis = Input.GetAxis ("Vertical");
		float xAxis = Input.GetAxis ("Horizontal");
		
		transform.position += new Vector3 (0f, hAxis*Time.deltaTime*speed, 0f);
		transform.Rotate(0f, xAxis * Time.deltaTime * speed, 0f);
	
	}
}
