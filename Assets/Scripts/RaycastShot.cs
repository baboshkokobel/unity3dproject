﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RaycastShot : MonoBehaviour {

	public GameObject Laser; 
	public Text shotCount;
	int n=0;

	void Update () {
		if (Input.GetButtonDown ("Fire1")) {
			n++;
			shotCount.text= n.ToString();
			Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
			RaycastHit hit;
			Physics.Raycast(ray,out hit);
			Debug.DrawRay (Camera.main.transform.position, Camera.main.transform.forward, Color.blue, 5f);


			if(hit.collider != null)
				Destroy(hit.collider.gameObject, 0.1f);
		}

	
	}
}
